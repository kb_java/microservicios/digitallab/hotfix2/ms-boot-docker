package academy.digitallab.store.baserx.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Comments {
    private List<String> comments;

    public Comments() {
        this.comments = new ArrayList<>();
    }

    public void addComments(String comment) {
        this.comments.add( comment);
    }
}

