package academy.digitallab.store.baserx;

import academy.digitallab.store.baserx.model.Comments;
import academy.digitallab.store.baserx.model.User;
import academy.digitallab.store.baserx.model.UserComment;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootApplication
public class BaserxApplication  implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BaserxApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		lab12_counter_pressure();
	}

	private void lab01(){
		Flux<String> names = Flux.just("Christian", "Salvador", "Juan", "Juan", "Denilsson", "Joel","Raul", "Porfirio", "Eduardo")
				.doOnNext(name -> {
							//System.out.println(name);
							if (name.isEmpty()){
								throw  new RuntimeException("nombre vacio");
							}
						}
				)
				.map( name -> {
					return name.toUpperCase();
				})
				//.filter(name ->name.equals("JUAN"))
				.map( name ->{
					return name + " ok ";
				})
				;
				//.doOnNext( System.out::println);


		names.subscribe(name -> log.info(name),
				err -> log.error(err.getMessage()),
				new Runnable() {
					@Override
					public void run() {
						log.info("Finalizo flujo");
					}
				}
		);
		//names.subscribe(log::info);
	}

	//Flujos desde lista
	private void  lab02_list(){
		List<String> listNames = new ArrayList<>();
		listNames.add("Christian");
		listNames.add("Salvador");
		listNames.add("Juan");
		listNames.add("Denilsson");
		listNames.add("Joel");
		listNames.add("Raul");
		listNames.add("Porfirio");

		Flux<User> users = Flux.fromIterable(listNames)
				.doOnNext(name -> {
							//System.out.println(name);
							if (name.isEmpty()){
								throw  new RuntimeException("nombre vacio");
							}
						}
				)
				.map( name -> {
					return name.toUpperCase();
				})
				.map(name ->{
					return User.builder().firstName(name).build();
				});

		users.subscribe(user -> log.info(user.toString()),
				err -> log.error(err.getMessage()),
				new Runnable() {
					@Override
					public void run() {
						log.info("Finalizo flujo");
					}
				}
		);

	}

	//flatmap
	private void  lab03_flatmap(){
		List<String> listNames = new ArrayList<>();
		listNames.add("Christian");
		listNames.add("Salvador");
		listNames.add("Juan");
		listNames.add("Denilsson");
		listNames.add("Joel");
		listNames.add("Raul Sanchez");
		listNames.add("Raul Perez");
		listNames.add("Porfirio");

		Flux<User> users = Flux.fromIterable(listNames)
				.doOnNext(name -> {
							//System.out.println(name);
							if (name.isEmpty()){
								throw  new RuntimeException("nombre vacio");
							}
						}
				)
				.map( name -> {
					return name.toUpperCase();
				})
				.map(name ->{
					return User.builder().firstName(name).build();
				})
				.map(user -> {
					if(user.getFirstName().split(" ").length>1){
						user.setLastName( user.getFirstName().split(" ")[1]);
						user.setFirstName( user.getFirstName().split(" ")[0]);
					}
					return user;
				})
				.flatMap( user -> {
					if (user.getFirstName().equals("RAUL")){
						return Mono.just(user);
					}else{
						return Mono.empty();
					}
				});

		users.subscribe(user -> log.info(user.toString()),
				err -> log.error(err.getMessage()),
				new Runnable() {
					@Override
					public void run() {
						log.info("Finalizo flujo");
					}
				}
		);

	}

	private void lab04_Flux_to_Mono(){
		List<String> listNames = new ArrayList<>();
		listNames.add("Christian");
		listNames.add("Salvador");
		listNames.add("Juan");
		listNames.add("Denilsson");
		listNames.add("Joel");
		listNames.add("Raul Sanchez");
		listNames.add("Raul Perez");
		listNames.add("Porfirio");

		 Flux.fromIterable(listNames)
				.doOnNext(name -> {
							//System.out.println(name);
							if (name.isEmpty()){
								throw  new RuntimeException("nombre vacio");
							}
						}
				)
				.map( name -> {
					return name.toUpperCase();
				}).collectList()
				.subscribe(nameList -> log.info(nameList.toString() ) );
	}



	//Combinando Flujos con Flatmap
	private  void lab05_stream_flatmap(){
		//Flujo User
		Mono<User> userMono =  Mono.fromCallable(()->  User.builder().firstName("Eduardo").lastName("Marchena").build() );

		//Flujo Comentarios
		Mono<Comments> commentsMono = Mono.fromCallable( () ->{
			Comments comments =  new Comments();
			comments.addComments("inicia el flujo");
			comments.addComments("programacion rectiva ");
			comments.addComments("ahora soy reactivo ");
			return comments;
		});
		//Aplanando el Flujo user y generamos nuevo flujo usuario comentario
		userMono.flatMap(u -> commentsMono.map( c -> new UserComment(u,c)))
				.subscribe(uc -> log.info(uc.toString()));
	}

	// Combinando Flujos zipWith
	private  void lab06_zipWith(){
		Mono<User> userMono =  Mono.fromCallable(()-> User.builder().firstName("Eduardo").lastName("Marchena").build());

		Mono<Comments> commentsMono = Mono.fromCallable( () ->{
			Comments comments =  new Comments();
			comments.addComments("inicia el flujo");
			comments.addComments("programacion rectiva ");
			comments.addComments("ahora soy reactivo ");
			return comments;
		});

		userMono.zipWith( commentsMono, (u, c) -> new UserComment( u,c) )
				.subscribe(uc -> log.info(uc.toString()));
	}

	//Rango
	private  void lab08_Rango(){
		Flux<Integer> rangos = Flux.range(0,4);

		Flux.just(1,2,3,4)
				.map(i -> (i*2))
				.zipWith(rangos, (n, r) ->String.format("Primer Flux %d, Segundo Flux %d", n,r) )
				.subscribe(log::info);
	}

	//Intervalos de tiempo
	private  void lab09_Interval(){
		Flux<Integer> rango = Flux.range(1,12);

		Flux<Long> interval = Flux.interval(Duration.ofSeconds(1));

		rango.zipWith(interval, (ra, in) -> ra)
				.doOnNext(i -> log.info(i.toString() ) )
				//.subscribe();
				.blockLast();

	}

	//Intervalos de tiempo Delay
	private  void lab10_Interval_DelayElements(){
		Flux<Integer> rango = Flux.range(1,12)
				.delayElements( Duration.ofSeconds(1))
				.doOnNext(i -> log.info(i.toString() ) );

		rango.blockLast();
	}


	private  void lab12_counter_pressure(){
		Flux<Integer> numbers= Flux.range(1,20)
				//.limitRate(5)
				.log();
		//.limitRate(5);
		//numbers.subscribe();

		numbers.subscribe(new Subscriber<Integer>() {
			private Subscription s;
			private  final Integer limit = 5;
			private   Integer count = 0;

			@Override
			public void onSubscribe(Subscription s) {
				this.s = s;
				s.request(limit);
			}

			@Override
			public void onNext(Integer integer) {
				count ++;
				if (count.equals(limit)){
					count = 0 ;
					s.request(limit);
				}
			}

			@Override
			public void onError(Throwable t) {

			}

			@Override
			public void onComplete() {

			}
		});

	}

}
