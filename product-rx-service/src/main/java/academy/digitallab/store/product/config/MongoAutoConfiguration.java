package academy.digitallab.store.product.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;


@Configuration
@EnableReactiveMongoRepositories(basePackages = "academy.digitallab.store.product")
public class MongoAutoConfiguration {


}
