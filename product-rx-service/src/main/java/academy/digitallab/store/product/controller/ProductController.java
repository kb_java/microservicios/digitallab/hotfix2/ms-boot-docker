package academy.digitallab.store.product.controller;

import academy.digitallab.store.product.model.document.Category;
import academy.digitallab.store.product.model.document.Product;
import academy.digitallab.store.product.model.edt.CategoryEDT;
import academy.digitallab.store.product.model.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;
import java.time.Duration;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping
    public Flux<Product>listProduct(){
        return productService.findAll();
    }

    @GetMapping(value="/delay")
    public Flux<Product>listProductDelay( @RequestParam(name = "categoryId", required = false) String categoryId){
        return  productService.findByCategory ( Category.builder().id(categoryId ).name(CategoryEDT.get(categoryId).getValue()).build())
                .delaySequence(Duration.ofSeconds(3));
    }

    @GetMapping(value="/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Product>listProductStream(@RequestParam(name = "categoryId", required = false) String categoryId){
        return  productService.findByCategory ( Category.builder().id(categoryId ).name(CategoryEDT.get(categoryId).getValue()).build())
                .delayElements(Duration.ofSeconds(1));
    }


    @GetMapping("/{id}")
    public Mono<ResponseEntity<Product>> getProduct(@PathVariable("id") String id){
        return productService.getProduct(id)
                .map(product -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(product)
                ).defaultIfEmpty(ResponseEntity.notFound().build());
    }
    @PostMapping
    public Mono<ResponseEntity<Product> >createProduct(@RequestBody Product product){
        return productService.createProduct( product)
                .map(productDB -> ResponseEntity.created(URI.create("/api/products/".concat(product.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(productDB)
                );
    }
    @PutMapping(value = "/{id}")
    public Mono<ResponseEntity<Product> >updateProduct(@PathVariable("id") String id, @RequestBody Product product){
        return productService.getProduct(id)
                .flatMap(productDB -> {
                    productDB.setDescription(product.getDescription());
                    productDB.setName(product.getName());
                    productDB.setCategory(product.getCategory());
                    return productService.updateProduct(productDB);
                }).map(productDB -> ResponseEntity.created(URI.create("/api/products/".concat(product.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(productDB)
                ).defaultIfEmpty(ResponseEntity.notFound().build());

    }
    @DeleteMapping(value = "/{id}")
    public Mono<ResponseEntity<Void>>  deleteProduct(@PathVariable("id") String id){
        return productService.getProduct(id)
                .flatMap(product -> {
                    return productService.deleteProduct(product)
                            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
                }).defaultIfEmpty(new ResponseEntity<Void>( HttpStatus.NOT_FOUND));
    }
}
