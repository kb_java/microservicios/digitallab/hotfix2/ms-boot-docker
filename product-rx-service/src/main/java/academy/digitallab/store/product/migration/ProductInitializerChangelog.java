package academy.digitallab.store.product.migration;

import academy.digitallab.store.product.model.document.Category;
import academy.digitallab.store.product.model.document.Product;
import academy.digitallab.store.product.model.edt.CategoryEDT;
import academy.digitallab.store.product.model.repository.ProductRepository;
import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import reactor.core.publisher.Flux;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Slf4j
@ChangeLog(order = "9999999")
public class ProductInitializerChangelog {
    private static final int INITIAL_BOOK= 50;
    private static final int INITIAL_FOOD= 50;

    private static final Faker faker = new Faker();
   // private static final Random random = new SecureRandom();


    @Transactional
    @ChangeSet(id = "20200826160000-data-initialize", order = "001", author = "digitallab")
    public void dataInitializer01(ProductRepository productRepository){
        java.lang.reflect.Proxy.getInvocationHandler(productRepository);

        Flux<Product> booksFlux = Flux.range(1, INITIAL_BOOK)
                .map(index -> getBooks(index));

        Flux<Product> foodsFlux = Flux.range(50, INITIAL_FOOD)
                .map(index -> getFoods(index));

        Flux<Product> products = booksFlux.mergeWith(foodsFlux);

        List<Product> elements = new ArrayList<>();
        productRepository.saveAll( products )
                .doOnNext(product -> {
                    log.info("Saved product with id {}", product.getId());

                }).subscribe(product -> {
                      log.info("finish Saved product with id {}", product.getId());
                      elements.add(product);
                });
        log.info(" =========================== " + elements.size() );
    }
    private static Product getBooks(Integer index) {
        return Product.builder()
                .id(index.toString())
                .category(Category.builder().id(CategoryEDT.BOOK.getKey()).name(CategoryEDT.BOOK.getValue()).build() )
                .name(faker.book().title())
                .description(faker.book().publisher())
                .price(faker.random().nextDouble() + faker.random().nextInt(1,100))
                .stock(faker.random().nextInt(1,10))
                .numberProduct(faker.number().digits(6))
                .status("CREATED")
                .build();
    }
    private static Product getFoods(Integer index) {
        return Product.builder()
                .id(index.toString())
                .category(Category.builder().id(CategoryEDT.FOOD.getKey()).name(CategoryEDT.FOOD.getValue()).build() )
                .name(faker.food().fruit())
                .description(faker.food().fruit())
                .price(faker.random().nextDouble() + faker.random().nextInt(1,100))
                .stock(faker.random().nextInt(1,10) )
                .numberProduct(faker.number().digits(6))
                .status("CREATED")
                .build();
    }

}
