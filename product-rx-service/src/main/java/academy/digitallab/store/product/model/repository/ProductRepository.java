package academy.digitallab.store.product.model.repository;

import academy.digitallab.store.product.model.document.Category;
import academy.digitallab.store.product.model.document.Product;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ProductRepository extends ReactiveMongoRepository<Product, String> {
    public Flux<Product> findByCategory(Category category);
}
