package academy.digitallab.store.product;

import academy.digitallab.store.product.model.document.Category;
import academy.digitallab.store.product.model.document.Product;
import academy.digitallab.store.product.model.repository.CategoryRepository;
import academy.digitallab.store.product.model.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootApplication
public class ProductRxServiceApplication   {

	public static void main(String[] args) {
		SpringApplication.run(ProductRxServiceApplication.class, args);
	}




}
