package academy.digitallab.store.product.migration;

import academy.digitallab.store.product.model.document.Category;
import academy.digitallab.store.product.model.edt.CategoryEDT;
import academy.digitallab.store.product.model.repository.CategoryRepository;
import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;

import reactor.core.publisher.Flux;

import java.util.stream.Stream;

@Slf4j
@ChangeLog(order = "1")
public class InitializerChangelog {
    private static final Faker faker = new Faker();

    @ChangeSet(id = "20200827161800-data-initialize", order = "001", author = "digitallab")
    public void dataInitializer01(CategoryRepository categoryRepository){
        java.lang.reflect.Proxy.getInvocationHandler(categoryRepository);

        Flux<Category> categoryFlux = Flux.fromStream(
                Stream.of(
                        Category.builder().id( CategoryEDT.BOOK.getKey()).name( CategoryEDT.BOOK.getValue()).build(),
                        Category.builder().id( CategoryEDT.CAT.getKey()).name( CategoryEDT.CAT.getValue() ).build(),
                        Category.builder().id( CategoryEDT.DOG.getKey()).name( CategoryEDT.DOG.getValue() ).build(),
                        Category.builder().id( CategoryEDT.FOOD.getKey()).name( CategoryEDT.FOOD.getValue() ).build()
                )
        );

        categoryRepository.saveAll (categoryFlux)
                .doOnNext(category -> log.info("Saved category with id {}", category.getId()))
                .subscribe();

    }
}
