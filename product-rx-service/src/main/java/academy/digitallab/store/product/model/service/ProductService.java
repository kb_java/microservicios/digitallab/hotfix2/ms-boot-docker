package academy.digitallab.store.product.model.service;

import academy.digitallab.store.product.model.document.Category;
import academy.digitallab.store.product.model.document.Product;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ProductService {
    public Flux<Product> findAll();
    public Flux<Product> findByCategory(Category category);
    public Mono<Product> getProduct(String id);
    public Mono<Product> createProduct( Product product);
    public Mono<Product>  updateProduct(Product product);
    public  Mono<Void>  deleteProduct(Product product);


    public Mono<Category> createCategory(Category category);
    public Mono<Category> updateCategory(Category category);
    public Mono<Category> getCategory(String id);
    public  Mono<Void> deleteCategory(Category category);
    public Flux<Category> findCategoryAll();
}
