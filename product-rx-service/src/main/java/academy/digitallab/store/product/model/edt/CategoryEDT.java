package academy.digitallab.store.product.model.edt;

import java.util.*;

public enum CategoryEDT {
    BOOK("01","BOOK"),
    CAT("02","CAT"),
    DOG("03","DOG"),
    FOOD("04","FOOD"),

            ;
    private String key;
    private String value;

    private static final List<CategoryEDT> list = new ArrayList<>();
    private static final Map<String, CategoryEDT> lookup = new HashMap<>();

    static {

        for (CategoryEDT s : EnumSet.allOf(CategoryEDT.class)) {
            list.add(s);
            lookup.put(s.getKey(), s);
        }
    }

    private CategoryEDT(String key, String value) {
        this.key = key;
        this.value = value;
    }
    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public static CategoryEDT get(String key){
        return lookup.get(key);
    }

    public static String getValue(String key){
        CategoryEDT obj= lookup.get(key);
        if(obj==null) {
            return null;
        }
        return obj.getValue();
    }
}
