package academy.digitallab.store.product.model.repository;

import academy.digitallab.store.product.model.document.Category;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends ReactiveMongoRepository<Category, String> {
}
