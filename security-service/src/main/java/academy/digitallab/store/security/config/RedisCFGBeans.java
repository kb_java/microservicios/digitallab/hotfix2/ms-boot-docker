package academy.digitallab.store.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
@RefreshScope
public class RedisCFGBeans {

    @Autowired
    private RedisCFG redisCFG;


    @RefreshScope
    @Bean
    JedisConnectionFactory jedisConnectionFactory() {
        JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
        System.out.println("serverHost:"+redisCFG.getServerHost());
        System.out.println("serverPort:"+redisCFG.getServerPort());

        jedisConFactory.setHostName(redisCFG.getServerHost());
        jedisConFactory.setPort(redisCFG.getServerPort());
        return jedisConFactory;
    }

    @RefreshScope
    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        final RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(jedisConnectionFactory());
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new GenericToStringSerializer<>(Object.class));
        return template;
    }
}
