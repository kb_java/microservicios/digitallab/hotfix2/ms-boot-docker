package academy.digitallab.store.security.domain.service;

import academy.digitallab.store.security.auth.TokenAuthenticationService;
import academy.digitallab.store.security.domain.model.TokenRedis;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@RefreshScope
public class RedisServiceImpl  implements RedisService{



    @Autowired
    private RedisTemplate<String, Object> template;


    @Override
    public void addToken(String token) {
        TokenRedis tokenRedis= TokenAuthenticationService.getExpireToken ( token );
        String key = doTokenKeySession(token);
        log.info("addToken => "+key);
        template.opsForValue().set(key, String.valueOf(tokenRedis.getUsername ()));
        Long nowTime = new Date(  ).getTime ();
        long session = tokenRedis.getDateTime () - nowTime;
        template.expire(key,session , TimeUnit.MILLISECONDS);

    }

    @Override
    public boolean existToken( String token) {
        log.info ( token );
        Boolean exist =false;
        exist= template.hasKey(doTokenKeySession(token));
        if (exist == null){
            return  false;
        }
        return exist;
    }
    private static String doTokenKeySession(String token){
        return "token:"+token;
    }
}
