package academy.digitallab.store.security.domain.model;

import lombok.Data;

@Data
public class TokenRedis {
    String username;
    Long dateTime;
}
