package academy.digitallab.store.gateway.service;

public interface RedisService {
    public boolean existToken( String token);
}
