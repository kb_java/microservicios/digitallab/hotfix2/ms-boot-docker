package academy.digitallab.store.gateway.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;

@Configuration
@EnableSwagger2WebFlux
public class SwaggerConfig {

    @Value(value = "${swagger.enabled}")
    Boolean swaggerEnabled;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).enable ( swaggerEnabled )
                .select()
                .apis(RequestHandlerSelectors.basePackage ("academy.digitallab.store.gateway" ))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(
                        new ApiInfoBuilder()
                                .version("1.0.1")
                                .title("Online store API")
                                .description("Documentation Online Store API v1.0")
                                .contact( new Contact("team","http://digitallab.com/", "team@digitallab.academy" ))
                                .build() );
    }
}
