package academy.digitallab.store.gateway.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


@Service
@Slf4j
@RefreshScope
public class RedisServiceImpl  implements RedisService{



    @Autowired
    private RedisTemplate<String, Object> template;




    @Override
    public boolean existToken( String token) {
        log.info ( token );
        Boolean exist =false;
        exist= template.hasKey(doTokenKeySession(token));
        if (exist == null){
            return  false;
        }
        return exist;
    }
    private static String doTokenKeySession(String token){
        return "token:"+token;
    }
}
