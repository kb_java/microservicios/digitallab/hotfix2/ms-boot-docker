package academy.digitallab.store.product;

import academy.digitallab.store.product.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Slf4j
@SpringBootApplication
public class ProductClientRxApplication  implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ProductClientRxApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		WebClient client = WebClient.create("http://localhost:8091");
//		log.info("init");
//
//		Flux<Product> books=client.get().uri("/api/products/delay?categoryId=01")
//                .retrieve()
//                .bodyToFlux(Product.class)
//                .doOnNext(product ->    log.info(product.getName() )
//            );
//		Flux<Product> books2=client.get().uri("/api/products/delay?categoryId=01")
//				.retrieve()
//				.bodyToFlux(Product.class)
//				.doOnNext(product ->    log.info(product.getName() )
//				);
//		Flux<Product> foods=client.get().uri("/api/products/delay?categoryId=04").retrieve().bodyToFlux(Product.class);
//
//		Flux<Product> foods2=client.get().uri("/api/products/delay?categoryId=04").retrieve().bodyToFlux(Product.class);
//
//		Flux<Product> all=Flux.merge(books,foods, books2,foods2);
//
//		all.subscribe(product -> log.info(product.getCategory().getName()+ ": "+   product.getName()));
//        log.info("finish");

		log.info("init");
		Flux<Product> books= client.get()
				.uri("/api/products/stream?categoryId=01")
				.accept(MediaType.TEXT_EVENT_STREAM)
				.retrieve()
				.bodyToFlux(Product.class);

		Flux<Product> foods= client.get()
				.uri("/api/products/stream?categoryId=04")
				.accept(MediaType.TEXT_EVENT_STREAM)
				.retrieve()
				.bodyToFlux(Product.class);

		Flux<Product> all=Flux.merge(books,foods);

		all .subscribe(product -> {
			log.info(product.getCategory().getName() +": "+ product.getName());
		});
		log.info("finish");
	};


}
