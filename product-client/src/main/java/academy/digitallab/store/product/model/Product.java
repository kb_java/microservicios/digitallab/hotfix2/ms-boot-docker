package academy.digitallab.store.product.model;

import lombok.Data;

@Data
public class Product {
    private String id;

    private String name;
    private String description;

    private Double stock;
    private Integer price;
    private String status;

    private Category category;
}
