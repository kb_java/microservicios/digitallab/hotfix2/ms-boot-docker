package academy.digitallab.store.product;

import academy.digitallab.store.product.client.ProductClient;
import academy.digitallab.store.product.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@EnableFeignClients
@SpringBootApplication
public class ProductClientApplication implements CommandLineRunner {

	@Autowired
	ProductClient productClient;

	public static void main(String[] args) {
		SpringApplication.run(ProductClientApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//long startTime = System.currentTimeMillis();
		log.info("init");
		List<Product> lista= new ArrayList<>();
		lista.addAll(getBooks());
		lista.addAll(getFoods());
		lista.addAll(getBooks());
		lista.addAll(getFoods());
		lista.forEach(product -> {
			log.info(product.getName());
		});
		log.info("finish");
	}

	private List<Product>  getBooks(){
		return	productClient.listProduct("01").getBody();

	}
	private List<Product>  getFoods(){
		return	productClient.listProduct("04").getBody();

	}


}
