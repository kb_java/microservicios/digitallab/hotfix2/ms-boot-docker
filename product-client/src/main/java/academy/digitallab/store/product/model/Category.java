package academy.digitallab.store.product.model;

import lombok.Data;

@Data
public class Category {
    private String id;
    private String name;

}
