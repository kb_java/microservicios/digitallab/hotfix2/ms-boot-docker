package academy.digitallab.store.product.client;

import academy.digitallab.store.product.model.Product;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "product-service", url = "http://localhost:8091")
@RequestMapping(value = "/api/products")
public interface ProductClient {


    @GetMapping("/delay")
    public ResponseEntity<List<Product>> listProduct(@RequestParam(name = "categoryId", required = false) String categoryId);

}