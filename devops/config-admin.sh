#!/bin/bash
echo "run config service"
docker run  -itd  --name config-service  -p 8081:8081 -e GIT_USER=$GIT_USER -e GIT_PASSWORD=$GIT_PASSWORD    --network=onlinestore registry.digitallab.academy:5001/config-service:$BUILD_VERSION

echo "run admin service"
docker run  -itd  --name admin-service -p 8086:8086  --network=onlinestore    registry.digitallab.academy:5001/admin-service:$BUILD_VERSION

echo "run redis service"
docker run  -itd  --name redis-service -p 8087:8087  --network=onlinestore registry.digitallab.academy:5001/redis-service:$BUILD_VERSION

